const gulp = require('gulp');
//Create allows multiple Servers or Proxys
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const purgecss = require('gulp-purgecss');
//SCSS



const paths = {
  //Every foloder in scss/ that has a scss file
  scss: 'src/scss/**/*.scss',
  allSrc: 'src/*.html',
  js: 'src/js/*.js',
  css: 'src/css',
  toCleancss: 'src/css/'
}



//gulp version 4 need to writte functions!
function style() {
  // 1. Where are the scss files --> paths Object!
  return gulp.src(paths.scss)
  // 2. pass that file through sass compiler
    .pipe(sass().on('error', sass.logError))
  // 3. where do I save the compiled css
    .pipe(gulp.dest(paths.css))
  // 4. stream change to all browser
    .pipe(browserSync.stream());
}


function cleancss() {
  // 1. select sass compiled css file
  return gulp.src('src/css/*.css')
  // 2. Purge unused CSS
    .pipe(purgecss({
      content: ['**/*.html']
    }))

    .pipe(autoprefixer({
      browsers:['last 5 versions'],
      cascasde: false
    }))
  //4. Replace cleaned file
    .pipe(gulp.dest(paths.toCleancss))
}

function watch() {
  console.log("SCSS compile and reload running...")
  browserSync.init({
    proxy: "http://127.0.0.1:8000"
  });
  gulp.watch(paths.scss, style);
  gulp.watch([paths.allSrc, paths.js]).on('change', browserSync.reload);
}



//Need to export since gulp 4
exports.style = style;
exports.cleancss = cleancss;
exports.watch = watch;
